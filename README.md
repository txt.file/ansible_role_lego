<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2021, Vieno Hakkerinen <vieno@hakkerinen.eu> -->
lego
=========

Get TLS certificates using DNS and [go-acme.github.io/lego](https://go-acme.github.io/lego) as ACME client. Not all lego DNS providers are tested. Help and pull requests welcome.

Requirements
------------

This roles needs an DNS service or server for the ACME dns-01 challenge. You need to agree to the terms of service of your selected ACME service provider. This role runs lego with ```--agree-tos```.

You need a file named ```lego_env.sh``` with the DNS credentials/configuration for your choosen DNS providers needed environment variables according to [go-acme.github.io/lego/dns/](https://go-acme.github.io/lego/dns/).

Role Variables
--------------

Following is a list of the required variables with sample values. There are no defaults.
```
acme_dns_provider: "rfc2136" # the lego supported DNS provider to use
acme_domains: # which domains should a certificate requested for; all domains will be part of one cert; only one cert is currently supported
  - "*.expample.net"
  - "example.net"
acme_email: "noc@example.net" # email for certificate abuse and renewal notifications
acme_server: "https://acme-staging-v02.api.letsencrypt.org/directory" # URL to the ACME directory
```

Dependencies
------------

- ansible.builtin
- community.general

Example Playbook
----------------

```
- hosts: servers
  become: yes
  roles:
    - txt_file.lego
  vars_files:
    - vars/acme.yml
```
Inside ```vars/acme.yml```:
```
acme_dns_provider: "rfc2136"
acme_domains:
  - "*.expample.net"
  - "example.net"
acme_email: "noc@example.net"
acme_server: "https://acme-staging-v02.api.letsencrypt.org/directory"
```
Inside ```lego_env.sh```:
```
export RFC2136_NAMESERVER=127.0.0.1
export RFC2136_TSIG_KEY=lego
export RFC2136_TSIG_ALGORITHM=hmac-sha256.
export RFC2136_TSIG_SECRET=YWJjZGVmZGdoaWprbG1ub3BxcnN0dXZ3eHl6MTIzNDU=
```


License
-------

Unlicense

Author Information
------------------

This role was created in 2021 by [Vieno Hakkerinen](https://www.hakkerinen.eu/).
